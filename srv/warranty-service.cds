using com.adbsafegate.project from '../db/warranty-model';

service WarrantyService {
  entity Warranty as projection on project.Warranty;
}
