namespace com.adbsafegate.project;

entity Warranty {
    KEY ID : UUID;
    ProjectID : String(20);
    Workpackage : String(15);
    WarrantyType : String(4);
    BondType : String(4);
    StartDate : Date ;
    EndDate : Date;
    Bank : String(40);
    BondNumber: Integer; 
    WarrantyValue: DecimalFloat;
    AutoDischarge : Boolean;
    Currency : String(3);
    Active : Boolean;
    Owner : String;
    NotifyBefore : Date;
}
